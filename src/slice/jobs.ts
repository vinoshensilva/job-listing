// Import libraries
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Import data
import jobs from "@/data/jobs";

// Import types
import JobType from "@/model/job";

export interface TJobsState {
    tags: string[],
    jobs: JobType[]
}

type AddTagPayload = {
    tag: string
}

type RemoveTagPayload = {
    tag: string
}

const initialState: TJobsState = {
    tags: [],
    jobs
}

const jobsSlice = createSlice({
    name: 'jobs',
    initialState,
    reducers: {
        addTag: (state: TJobsState, { payload }: PayloadAction<AddTagPayload>) => {
            const { tag } = payload;

            const exists = state.tags.includes(tag)

            if (!exists) {
                state.tags.push(tag);
            }
        },
        removeTag: (state: TJobsState, { payload }: PayloadAction<RemoveTagPayload>) => {
            const { tag } = payload;

            const tagIndex = state.tags.findIndex((value) => { return value === tag });

            if (tagIndex !== -1) {
                state.tags.splice(tagIndex, 1);
            }
        },
        clearTags: (state: TJobsState) => {
            state.tags = [];
        }
    }
})

export const { addTag, clearTags, removeTag } = jobsSlice.actions;

export default jobsSlice.reducer;