// Techs
export const CSS = 'CSS';
export const JAVASCRIPT = 'JavaScript';
export const HTML = 'HTML';
export const PYTHON = 'Python';
export const RUBY = 'Ruby';
export const REACT = 'React';
export const DJANGO = 'Django';
export const ROR = 'RoR';

// Tools
export const SASS = 'Sass';
export const VUE = 'Vue';

// Level
export const SENIOR = 'Senior';
export const JUNIOR = 'Junior';
export const MIDWEIGHT = 'Midweight';

// Role
export const FRONTEND = 'Frontend';
export const BACKEND = 'Backend';
export const FULLSTACK = 'Fullstack';