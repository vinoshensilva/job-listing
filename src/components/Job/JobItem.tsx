// Import model
import JobType from "@/model/job"
import TagItem from "../Tag/TagItem";
import Tags from "../Tag/Tags";

type JobItemProps = {
    job: JobType
}

const JobItem = ({ job }: JobItemProps): JSX.Element => {

    const {
        logo,
        company,
        position,
        featured,
        new: NewJob,
        postedAt,
        contract,
        location,
        languages,
        tools,
        level,
        role
    } = job;

    const tags = [
        ...languages,
        ...tools,
        level,
        role
    ]

    return (
        <div className=" bg-white shadow-md p-4 rounded-sm flex flex-col mobile:flex-row gap-3 mobile:gap-2 border-l-4 border-primary">
            <div className="flex w-1/2">
                <div className="flex items-start gap-2">
                    <img src={logo} className='mobile:w-18 mobile:h-18 mx-auto' alt={`${company} Logo`} />
                    <div className="h-full flex flex-col justify-between gap-1">
                        <div className="flex gap-2">
                            <p className="text-primary text-sm">
                                {company}
                            </p>
                            {NewJob && <div className="badge bg-primary p-1 px-2 text-sm">New</div>}
                            {featured && <div className="badge bg-black p-1 px-2 text-sm">Featured</div>}
                        </div>
                        <p className="text-black">
                            {position}
                        </p>
                        <div className="flex gap-2 text-gray-400 text-sm text-center">
                            <p>
                                {postedAt}
                            </p>
                            <p>
                                .
                            </p>
                            <p>
                                {contract}
                            </p>
                            <p>
                                .
                            </p>
                            <p>
                                {location}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex-grow flex items-center">
                <Tags tags={tags} />
            </div>
        </div >
    )
}

export default JobItem