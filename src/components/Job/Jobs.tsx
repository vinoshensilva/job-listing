// Import types
import { TStore } from "@/store";

// Import components
import JobItem from "@/components/Job/JobItem";

// Import libraries
import { useSelector } from "react-redux"

const Jobs = (): JSX.Element => {
    const jobs = useSelector((state: TStore) => state.jobsReducer.jobs);
    const tags = useSelector((state: TStore) => state.jobsReducer.tags);

    const filteredJobs = jobs.filter((job) => {
        const {
            languages,
            tools,
            level,
            role
        } = job;

        const jobTags = [
            ...languages,
            ...tools,
            level,
            role
        ]

        return tags.every((tag) => jobTags.includes(tag));
    })

    return (
        <div className="mx-auto flex flex-col gap-3">
            {filteredJobs.map((job) => (
                <JobItem key={job.id} job={job} />
            ))}
        </div>
    )
}

export default Jobs