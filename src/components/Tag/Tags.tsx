// Import types
import { TStore } from "@/store"

// Import libraries
import { useSelector, useDispatch } from "react-redux"

// Import actions
import { addTag } from "@/slice/jobs";

//Import components 
import TagItem from "@/components/Tag/TagItem"

export type TagsProps = {
    tags: string[]
}

const Tags = ({ tags }: TagsProps): JSX.Element => {
    const dispatch = useDispatch();

    const addTagHandler = (tag: string) => {
        dispatch(addTag({ tag }));
    }

    return (
        <div className="w-full flex gap-y-3 gap-2 flex-wrap">
            {
                tags.map((tag) => (
                    <TagItem
                        key={tag}
                        text={tag}
                        onClick={addTagHandler.bind(null, tag)}
                    />
                ))
            }
        </div>
    )
}

export default Tags