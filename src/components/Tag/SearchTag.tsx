// Import components
import TagItem, { TagItemProps } from "@/components/Tag/TagItem"

interface SearchTagProps extends TagItemProps {
    onClickRemove: () => void
}

const SearchTag = ({ onClickRemove, text, onClick }: SearchTagProps): JSX.Element => {
    return (
        <div className="flex bg-neutral rounded-md">
            <TagItem onClick={onClick} text={text} />
            <div>
                <button
                    onClick={onClickRemove}
                    className="btn-primary p-1 rounded-r-sm hover:btn-secondary h-full w-full"
                >
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    )
}

export default SearchTag