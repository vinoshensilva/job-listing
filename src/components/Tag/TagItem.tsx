export interface TagItemProps {
    text: string,
    onClick?: () => void
}

const TagItem = ({ text, onClick }: TagItemProps): JSX.Element => {
    return (
        <button
            onClick={onClick}
            className="px-2 group flex items-center bg-neutral hover:bg-primary"
        >
            <p className="text-primary group-hover:text-white">
                {text}
            </p>
        </button>
    )
}

export default TagItem