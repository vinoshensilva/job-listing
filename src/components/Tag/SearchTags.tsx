// Import libraries
import { useDispatch } from "react-redux"

// Import Type
import { TagsProps } from "@/components/Tag/Tags"

// Import Components
import SearchTag from "@/components/Tag/SearchTag"
import { removeTag } from "@/slice/jobs";

const SearchTags = ({ tags }: TagsProps): JSX.Element => {
    const dispatch = useDispatch();

    const removeTagHandler = (tag: string) => {
        dispatch(removeTag({ tag }))
    }

    return (
        <div className="flex flex-wrap gap-2 gap-y-3">
            {
                tags.map((tag) => (
                    <SearchTag
                        key={tag}
                        text={tag}
                        onClickRemove={removeTagHandler.bind(null, tag)}
                    />
                ))
            }
        </div>
    )
}

export default SearchTags