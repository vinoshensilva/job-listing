
const Header = (): JSX.Element => {
    return (
        <div
            className="h-20 bg-primary grid grid-flow-col"
        >
            <div className="hidden mobile:flex bg-cover bg-no-repeat bg-header-desktop" />
            <div className="flex mobile:hidden bg-repeat-x bg-contain bg-header-mobile" />
        </div>
    )
}

export default Header