import Tags from "@/components/Tag/Tags"
import { clearTags } from "@/slice/jobs"
import { TStore } from "@/store"
import { useDispatch, useSelector } from "react-redux"
import SearchTags from "../Tag/SearchTags"

const FilterInput = (): JSX.Element => {
    const tags = useSelector((state: TStore) => state.jobsReducer.tags);

    const dispatch = useDispatch();

    const clearHandler = (): void => {
        dispatch(clearTags());
    }

    return (
        <div className="flex shadow-md -translate-y-1/2 mx-auto gap-2 p-3 bg-white  rounded-md">
            <div className="w-5/6">
                <SearchTags tags={tags} />
            </div>
            <div className="w-1/6 flex justify-center items-center">
                <button onClick={clearHandler} className="hover:text-primary hover:underline text-secondary">
                    Clear
                </button>
            </div>
        </div>
    )
}

export default FilterInput