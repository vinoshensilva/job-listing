// Import components
import Header from "@/components/UI/Header"
import Jobs from "@/components/Job/Jobs"
import FilterInput from "@/components/UI/FilterInput"

function App() {
  return (
    <div className="text-[15px] h-screen">
      <div className="h-full">
        <Header />

        <div className='bg-neutral pb-10'>
          <div className="max-w-[1000px] w-5/6 mx-auto">
            <FilterInput />
            <Jobs />
          </div>
        </div>
      </div>
    </div>
  )
}

export default App