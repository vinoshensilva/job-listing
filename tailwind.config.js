/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    screens: {
      'mobile': '376px',
      'desktop': '1440px'
    },
    extend: {
      backgroundImage: {
        'header-mobile': "url('/images/bg-header-mobile.svg')",
        'header-desktop': "url('/images/bg-header-desktop.svg')"
      },
      fontFamily: {
        'LeagueSpartan': ['League Spartan', 'sans-serif']
      }
    },
  },
  daisyui: {
    themes: [
      {
        light: {
          ...require("daisyui/src/colors/themes")["[data-theme=light]"],
          primary: '#5BA4A4',
          secondary: '#155e75',
          neutral: '#EFFAFA'
        },
      },
    ],
  },
  plugins: [require("daisyui")],
}